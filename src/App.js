import React from 'react';
import './App.css';

import Slider from './tools/Slider/slider';
import Checkbox from './tools/checkboxToggleSlider/checkbox';



const audioSrc = [
  {
    name: '111',
    id: 'Q',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3',
  },
  {
    name: '222',
    id: 'W',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3',
  },
  {
    name: '333',
    id: 'E',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3',
  },
  {
    name: '444',
    id: 'A',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3',
  },
  {
    name: '555',
    id: 'S',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Dry_Ohh.mp3',
  },
  {
    name: '666',
    id: 'D',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Bld_H1.mp3',
  },
  {
    name: '777',
    id: 'Z',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3',
  },
  {
    name: '888',
    id: 'X',
    url:'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3',
  },
  {
    name: '999',
    id: 'C',
    url:'https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3',
  },
]



class App extends React.Component {
  state = {
    power: true,
    txt: '',
    vol: 0.5,
  }

  handlePower = () => {
    this.setState({ power: !this.state.power });
  };

  handleClickDrum = ( letter ) => {
    if( this.state.power ){
      let clip = document.querySelector(`#${letter}`);
      clip.volume = this.state.vol;
      clip.play();

      let audio = audioSrc.filter( audio => audio.id === letter)[0];
      
      this.setState({
        txt: audio.name,
      });
    }
    else{
      this.setState({
        txt: '',
      });
    }
  };

  handleChangeVolume = ( e ) => {
    let newVol = e.target.value / 100;
    this.setState({ vol: newVol });
  };

  handleClickKeyboard = (e) => {
    let letter = String.fromCharCode(e.keyCode);
    this.handleClickDrum(letter);
  };
  
  componentDidMount() {
    document.addEventListener('keydown', this.handleClickKeyboard);
  }

  render() { 
    return (
      <div className="App">
        <div id="drum-machine">
          <div id='buttons'  >
            <button id='btnQ' className='drum-pad'
                    onClick={() => this.handleClickDrum('Q')}>Q
              <audio id="Q" className='clip' src={audioSrc[0]['url']}></audio>
            </button>
            <button id='btnW' className='drum-pad'
                    onClick={() => this.handleClickDrum('W')}>W
              <audio id="W" className='clip' src={audioSrc[1]['url']}></audio>
            </button>
            <button id='btnE' className='drum-pad'
                    onClick={() => this.handleClickDrum('E')}>E
              <audio id="E" className='clip' src={audioSrc[2]['url']}></audio>
            </button>

            <button id='btnA' className='drum-pad'
                    onClick={() => this.handleClickDrum('A')}>A
              <audio id="A" className='clip' src={audioSrc[3]['url']}></audio>
            </button>
            <button id='btnS' className='drum-pad'
                    onClick={() => this.handleClickDrum('S')}>S
              <audio id="S" className='clip' src={audioSrc[4]['url']}></audio>
            </button>
            <button id='btnD' className='drum-pad'
                    onClick={() => this.handleClickDrum('D')}>D
              <audio id="D" className='clip' src={audioSrc[5]['url']}></audio>
            </button>

            <button id='btnZ' className='drum-pad' 
                    onClick={() => this.handleClickDrum('Z')}>Z
              <audio id="Z" className='clip' src={audioSrc[6]['url']}></audio>
            </button>
            <button id='btnX' className='drum-pad' 
                    onClick={() => this.handleClickDrum('X')}>X
              <audio id="X" className='clip' src={audioSrc[7]['url']}></audio>
            </button>
            <button id='btnC' className='drum-pad'
                    onClick={() => this.handleClickDrum('C')}>C
              <audio id="C" className='clip' src={audioSrc[8]['url']}></audio>
            </button>
          </div>

          <div id='controls'>
            <div id='display'>{this.state.txt}</div>
            <Slider changed={(e) => this.handleChangeVolume(e)} />
            <Checkbox id='power' clicked={this.handlePower} >On / Off</Checkbox>  
          </div>
        </div>
      </div>
    );
  }
}


export default App;
