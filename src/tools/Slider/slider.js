import React from 'react';
import './slider.css';

const Slider = (props) => {
    return (
        <div className="volume-container">
            <input type="range" min="0" max="100" step="1" onChange={props.changed} className="volume-slider" id="myRange" />
        </div>
    );
}
 
export default Slider;