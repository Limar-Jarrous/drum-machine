import React from 'react';
import './checkbox.css';

const Checkbox = (props) => {
    return (
        <div className="checkbox-container" >
            <label className="switch">
                <input type="checkbox" id="checkbox"  onClick={props.clicked}/>
                <div className="checkbox-slider round"></div>
            </label>
            {props.children}
        </div>
    );
}
 
export default Checkbox;